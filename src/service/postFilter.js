export default class PostFilter {
    static apiFilter(postsArr, authorArr, value) {
        if (value) {
            let filterArr = []
            const authorId = authorArr.filter((author) => {
                if (author.username.toLowerCase().search(value) === 0) {
                    return author.username.toLowerCase()
                }
            })
            postsArr.forEach((post) => {
                authorId.filter((author) => {
                    if (post.userId === author.id) {
                        filterArr = [...filterArr, post]
                    }
                })
            })
            return filterArr;
        } else {
            return postsArr
        }
    }
}
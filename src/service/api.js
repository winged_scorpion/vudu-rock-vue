import axios from "axios";

export default class ApiService {
    static getPost() {
        return axios
            .get('http://jsonplaceholder.typicode.com/posts')
            .then(response => {
                return response;
            })
    }

    static getAuthor() {
        return axios
            .get('http://jsonplaceholder.typicode.com/users')
            .then(response => {
                return response.data.map(function (task) {
                    return {
                        id: task.id,
                        username: task.username
                    }
                });
            })
    }
}
